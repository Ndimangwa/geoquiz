package tz.co.zoomtong.geoquiz

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //Now deal with Fragment
        val frag = supportFragmentManager.findFragmentById(R.id.fragmentHolder)
        if (frag == null)   {
            supportFragmentManager
                .beginTransaction()
                .add(R.id.fragmentHolder, QuizFragment())
                .commit()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }
}
