package tz.co.zoomtong.geoquiz

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class CheatActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //Dealing with Fragment
        val frag = supportFragmentManager.findFragmentById(R.id.fragmentHolder)
        if (frag == null)   {
            supportFragmentManager
                .beginTransaction()
                .add(R.id.fragmentHolder, CheatFragment())
                .commit()
        }
    }

    companion object    {
        fun newIntent(packageContext: Context, answerIsTrue: Boolean): Intent {
            return CheatFragment.newIntent(packageContext, answerIsTrue)
        }
    }
}
