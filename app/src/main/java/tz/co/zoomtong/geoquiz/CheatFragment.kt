package tz.co.zoomtong.geoquiz

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider

class CheatFragment : Fragment() {
    private var answerIsTrue = false
    private lateinit var answerTextView: TextView
    private lateinit var showAnswerButton: Button
    private val cheatViewModel: CheatViewModel by lazy {
        ViewModelProvider(activity!!).get(CheatViewModel::class.java)
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //Loading the saved answer from the CheatViewModel
        setAnswerShownResult(cheatViewModel.answerShownResult) //Previous answer, default false
        val view1 = inflater.inflate(R.layout.cheat_fragment, container, false)

        answerIsTrue = activity!!.intent.getBooleanExtra(EXTRA_ANSWER_IS_TRUE, false)

        answerTextView = view1.findViewById(R.id.answer_text_view) as TextView
        //Load previous values if saved
        if (cheatViewModel.answerText != -1)   {
            answerTextView.setText(cheatViewModel.answerText)
        }
        showAnswerButton = view1.findViewById(R.id.show_answer_button) as Button
        showAnswerButton.setOnClickListener{
            val answerText = when{
                answerIsTrue -> R.string.true_button
                else -> R.string.false_button
            }
            cheatViewModel.answerText = answerText
            answerTextView.setText(answerText)
            //update model
            cheatViewModel.answerShownResult = true
            setAnswerShownResult(true)
        }

        return view1
    }
    private fun setAnswerShownResult(isAnswerShown: Boolean)    {
        val data = Intent().apply {
            putExtra(EXTRA_ANSWER_SHOWN, isAnswerShown)
        }
        activity!!.setResult(Activity.RESULT_OK, data)
    }
    companion object {
        private const val EXTRA_ANSWER_IS_TRUE = "tz.co.zoomtong.geoquiz.answer_is_true"
        public const val EXTRA_ANSWER_SHOWN = "tz.co.zoomtong.geoquiz.answer_show"

        fun newIntent(packageContext: Context, answerIsTrue: Boolean): Intent {
            return Intent(packageContext, CheatActivity::class.java).apply{
                //Call method of the returned Intent, this keywork can also apply
                putExtra(EXTRA_ANSWER_IS_TRUE, answerIsTrue)
            }
        }
    }
}