package tz.co.zoomtong.geoquiz

import android.util.Log
import androidx.lifecycle.ViewModel

class QuizViewModel: ViewModel() {
    private val questionBank: List<Question> = listOf(
        Question(R.string.question_australia, true, false),
        Question(R.string.question_oceans, true, false),
        Question(R.string.question_mideast, false, false),
        Question(R.string.question_africa, false),
        Question(R.string.question_americas, true),
        Question(R.string.question_asia, true))

    private var currentIndex = 0
    var isCheater = false

    fun getCurrentIndex(): Int  {
       return currentIndex
    }
    fun setCurrentIndex(index: Int) {
        currentIndex = index
    }
    val currentQuestionText: Int
        get() = questionBank[currentIndex].textResId
    val currentQuestionAnswer: Boolean
        get() = questionBank[currentIndex].answer
    val isCurrentQuestionAnswered: Boolean
        get() = questionBank[currentIndex].answered
    fun setCurrentQuestionAnswered(answered: Boolean)    {
        questionBank[currentIndex].answered = answered
    }
    val isCurrentQuestionAnsweredCorrectly: Boolean
        get() = questionBank[currentIndex].answeredCorrectly
    fun setCurrentQuestionAnsweredCorrectly(answeredCorrectly: Boolean) {
        questionBank[currentIndex].answeredCorrectly = answeredCorrectly
    }
    fun moveToNext()    {
        currentIndex = (currentIndex + 1) % questionBank.size
    }
    fun moveToPrevious()    {
        currentIndex = (questionBank.size + currentIndex - 1) % questionBank.size
    }
    fun isQuizCompleted(): Boolean  {
        var isAllAnswered = true
        for (question1 in questionBank) {
            if (! question1.answered)   {
                isAllAnswered = false
                break
            }
        }
        return isAllAnswered
    }

    fun getQuizGrade(): Float  {
        val quizSize = questionBank.size
        if (quizSize == 0) {
            return 0f
        }

        var correctAnswers: Int = 0
        for (question1 in questionBank) {
            if (question1.answeredCorrectly) correctAnswers++
        }

        return ((correctAnswers.toFloat() * 100) / quizSize)
    }
}