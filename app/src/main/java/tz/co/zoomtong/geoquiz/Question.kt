package tz.co.zoomtong.geoquiz

import androidx.annotation.StringRes

data class Question(@StringRes val textResId: Int, val answer: Boolean, var answered: Boolean = false, var answeredCorrectly: Boolean = false)