package tz.co.zoomtong.geoquiz

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider


class QuizFragment: Fragment() {
    //Define here all controls which are available in UI
    private lateinit var trueButton1: Button
    private lateinit var falseButton1: Button
    private lateinit var prevButton1: Button
    private lateinit var nextButton1: Button
    private lateinit var cheatButton1: Button
    private lateinit var questionTextView1: TextView
    //ViewModel
    private val quizViewModel: QuizViewModel by lazy {
        ViewModelProvider(activity!!).get(QuizViewModel::class.java)
    }

    private fun updateQuestion()    {
        questionTextView1.setText(quizViewModel.currentQuestionText)
        updateUIButtons()

        //Now Display Grade
        if (quizViewModel.isQuizCompleted())  {
            Toast.makeText(activity!!, "Completed!!! Score is ${quizViewModel.getQuizGrade()}%", Toast.LENGTH_LONG).show()
        }
    }

    private fun checkAnswer(userAnswer: Boolean)    {
        val correctAnswer = quizViewModel.currentQuestionAnswer

        /*val messageResId = if(userAnswer == correctAnswer)  {
            quizViewModel.setCurrentQuestionAnsweredCorrectly(true)
            R.string.correct_toast
        } else {
            quizViewModel.setCurrentQuestionAnsweredCorrectly(false)
            R.string.incorrect_toast
        }*/
        quizViewModel.setCurrentQuestionAnsweredCorrectly(false)
        val messageResId = when{
            quizViewModel.isCheater -> R.string.judgment_toast
            userAnswer == correctAnswer ->  {
                quizViewModel.setCurrentQuestionAnsweredCorrectly(true)
                R.string.correct_toast
            }
            else -> R.string.incorrect_toast
        }

        quizViewModel.setCurrentQuestionAnswered(true)
        Toast.makeText(activity!!, messageResId, Toast.LENGTH_LONG).show()
    }

    private fun moveToNextQuestion()    {
        quizViewModel.moveToNext()
        updateQuestion()
    }



    private fun updateUIButtons()   {
        val isQuestionAlreadyAnswered = quizViewModel.isCurrentQuestionAnswered
        trueButton1.isEnabled = ! isQuestionAlreadyAnswered
        falseButton1.isEnabled = ! isQuestionAlreadyAnswered
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d(TAG, "onCreateView() is called")
        //Reloading currentIndex
        val currentIndex = savedInstanceState?.getInt(KEY_INDEX, 0)?: 0
        quizViewModel.setCurrentIndex(currentIndex)

        val view1 = inflater.inflate(R.layout.quiz_fragment, container, false)
        //Handling Events
        trueButton1 = view1.findViewById(R.id.true_button) as Button
        falseButton1 = view1.findViewById(R.id.false_button) as Button
        prevButton1 = view1.findViewById(R.id.prev_button) as Button
        nextButton1 = view1.findViewById(R.id.next_button) as Button
        cheatButton1 = view1.findViewById(R.id.cheat_button) as Button
        questionTextView1 = view1.findViewById(R.id.question_text_view) as TextView

        trueButton1.setOnClickListener{
            checkAnswer(true)
        }

        falseButton1.setOnClickListener{
            checkAnswer(false)
        }

        nextButton1.setOnClickListener{
            moveToNextQuestion()
        }

        questionTextView1.setOnClickListener{
            moveToNextQuestion()
        }

        prevButton1.setOnClickListener{
            quizViewModel.moveToPrevious()
            updateQuestion()
        }
        cheatButton1.setOnClickListener{
            //start CheatActivity
            val answerIsTrue = quizViewModel.currentQuestionAnswer
            val intent = CheatActivity.newIntent(activity!!, answerIsTrue)
            //startActivity(intent)
            startActivityForResult(intent, REQUEST_CODE_CHEAT)
        }
        updateQuestion()

        return view1
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        Log.d(TAG, "onSaveInstanceState() called")
        outState.putInt(KEY_INDEX, quizViewModel.getCurrentIndex())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != Activity.RESULT_OK)   {
            return
        }
        if (requestCode == REQUEST_CODE_CHEAT)  {
            quizViewModel.isCheater = data?.getBooleanExtra(CheatFragment.EXTRA_ANSWER_SHOWN, false)?: false
        }
    }
    companion object{
        private const val KEY_INDEX = "tz.co.zoomtong.geoquiz_key_index"
        private const val TAG = "QuizFragment"
        private const val REQUEST_CODE_CHEAT = 0
    }
}